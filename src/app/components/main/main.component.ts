import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { HeroesServiceService } from 'src/app/services/heroes-service.service';
import { PartialObserver, fromEvent } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { hero, partialHero } from 'src/app/models/hero-model';
import { SearchService } from 'src/app/services/search.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {

  heroes: partialHero[] = [];
  originalData: partialHero[] = [];
  private completeList: partialHero[] = [];
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;

  constructor(
    private hService: HeroesServiceService,
    private searchSevice: SearchService,
    private router: Router
    ) {
    this.getHeroes();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initSearcher();
    this.getFiltered();
  }

  ngOnDestroy() {

  }

  getHeroes() {
    this.hService.getHeroes();
    this.hService.heroes$.subscribe(result => this.heroes = result);
  }

  initSearcher() {
    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      pluck('srcElement','value')
    ).subscribe(( value: string ) => this.searchSevice.filter(value));

  }

  getFiltered() {
    this.searchSevice.filtered$.subscribe(filtered => {
     this.heroes = filtered;
    });
  }

  get showInfo() {
    return this.heroes.length ? true : false;
  }

  goToHero( id: string ) {
    this.router.navigate(['details', id]);
  }

}
