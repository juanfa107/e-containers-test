import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesServiceService } from 'src/app/services/heroes-service.service';
import { PartialObserver } from 'rxjs';
import { hero } from 'src/app/models/hero-model';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  dataHero: hero;

  constructor(
    // tslint:disable-next-line: no-shadowed-variable
    private ActivatedRoute: ActivatedRoute,
    private hservice: HeroesServiceService,
    private router: Router
  ) {
    this.getId();
  }

  ngOnInit() {
  }

  getId() {
    this.ActivatedRoute.params.subscribe(params => {
      this.getInfo(params.idHero);
    });
  }

  getInfo( idHero: string ){

    const observer: PartialObserver<hero> = {
      next: (hero: hero) => this.dataHero = hero,
      error: (err) => {
        this.router.navigateByUrl('main');
      }
    };

    this.hservice.getHeroById(idHero).subscribe(observer);
  }

  keys(object){
    if ( object ) {
      return Object.keys(object);
    }
  }

}
