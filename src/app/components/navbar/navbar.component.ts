import { Component, OnInit } from '@angular/core';
import { HeroesServiceService } from 'src/app/services/heroes-service.service';

interface Section {
  title: string;
  route: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  logo = '../../assets/img/logo.png';

  options: Section[] = [
    {
      title: 'About',
      route: 'about'
    },
    {
      title: 'Main',
      route: 'main'
    }
  ];

  constructor(private hservice: HeroesServiceService) { }

  ngOnInit() {
  }

  getMore(){
    this.hservice.randomizerHeroes();
  }
}
