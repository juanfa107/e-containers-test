import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, zip, PartialObserver, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { hero, partialHero } from '../models/hero-model';

@Injectable({
  providedIn: 'root'
})
export class HeroesServiceService {

  constructor(private _httclient: HttpClient) { 
  }

  private BASE_URL = 'https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/';
  private heroes: partialHero[] = [];
  private completeList: partialHero[] = [];

  heroesSubject = new BehaviorSubject<partialHero[]>([]);
  heroes$ = this.heroesSubject.asObservable();

  // it doesn't work because ids don't exist :()
  // getHeroesFailed(){
  //   const requests: Observable<any>[] = [];
  //   const ids: number[] = this.generateRadomNumbers();
  //   ids.forEach(id => {
  //     const req = this._httclient.get(`${this.BASE_URL}id/${id}.json`);
  //     requests.push(req);
  //   });
  //   return zip(...requests).subscribe(console.log);
  // }

  // function to generate 20 random numbers to use like heroes's ids
  generateRadomNumbers(){
    const numbers: number[] = [];
    while ( numbers.length < 20 ) {
      const num = Math.floor(Math.random() * (563));
      if ( !numbers.includes(num) ) {
        numbers.push(num);
      }
    }
    return numbers;
  }

  getHeroes(){
    if( !this.heroes.length ){
      const observer: PartialObserver<any> = {
        next: (result) => {
          this.completeList = result;
          this.randomizerHeroes(result);
        },
        error: (err) => console.log({err}),
        complete: () => {}
      };
      this._httclient.get(`${this.BASE_URL}all.json`).pipe(
        map(
          ( heroes: hero[] ) => {
            return heroes.map(
              (hero: hero) => {
                return {
                  id: hero.id,
                  image: hero.images.sm,
                  name: hero.name,
                  info: {
                    intelligence: hero.powerstats.intelligence,
                    speed: hero.powerstats.speed,
                    power: hero.powerstats.power,
                    gender: hero.appearance.gender,
                    race: hero.appearance.race
                  }
                };
              }
            )
          }
        )
      ).subscribe( observer );
    }
  }

  randomizerHeroes(heroes?) {
    heroes = heroes || this.completeList;
    const tmpHeroes = [];
    const positions: number[] = this.generateRadomNumbers();
    positions.forEach(pos => {
      tmpHeroes.push(heroes[pos]);
    });

    this.setHeroes(tmpHeroes);
  }

  setHeroes(heroes: partialHero[]){
    this.heroes = heroes;
    this.heroesSubject.next(this.heroes);
  }

  getHeroById(id: string){
    return this._httclient.get(`${this.BASE_URL}id/${id}.json`);
  }

}
