import { Injectable } from '@angular/core';
import { HeroesServiceService } from './heroes-service.service';
import { partialHero } from '../models/hero-model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private originalData = [];
  private filterSubject = new BehaviorSubject<partialHero[]>([]);
  filtered$ = this.filterSubject.asObservable();

  constructor(private hService: HeroesServiceService) {
    this.hService.heroes$.subscribe(datos => {
      this.originalData = datos;
    });
  }

  filter(value: string){
    value = value.trim().toLowerCase();
    const filtered = this.originalData.filter( (hero: partialHero) => {
      return hero.name.toLowerCase().includes(value) || this.findInInfo(hero , value);
    });
    this.filterSubject.next(filtered);
  }

  findInInfo( hero: partialHero, value: string){
    if ( hero.info.gender ) {
      if ( hero.info.gender.toLowerCase().includes(value) ) {
        return true;
      }
    }
    if ( hero.info.race ) {
      if ( hero.info.race.toLowerCase().includes(value) ) {
        return true;
      }
    }
    return false;
  }

}
