import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './components/details/details.component';
import { MainComponent } from './components/main/main.component';
import { AboutComponent } from './components/about/about.component';


const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'main', component: MainComponent},
  { path: 'details/:idHero', component: DetailsComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'main'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
