import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'haveValue'
})
export class HaveValuePipe implements PipeTransform {

  transform(value: any): any {
    if(!value){
      return 'Unknown';
    }
    return value;
  }

}
