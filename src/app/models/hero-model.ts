// tslint:disable-next-line: class-name
interface hero {
    id: number;
    name: string;
    slug: string;
    powerstats: {
        intelligence: number;
        strength: number;
        speed: number;
        durability: number;
        power: number;
        combat: number;
    };
    appearance: Appearance;
    biography: Biography;
    work: Work;
    connections: Connections;
    images: Images;
}


interface Appearance {
    gender: string;
    race: string;
    height: string[];
    weight: string[];
    eyeColor: string;
    hairColor: string;
};

interface Biography {
    fullName: string;
    alterEgos: string;
    aliases: string[];
    placeOfBirth: string;
    firstAppearance: string;
    publisher: string;
    alignment: string;
};

interface Work {
    occupation: string;
    base: string;
};

interface Connections {
    groupAffiliation: string;
    relatives: string;
};

interface Images{
    xs: string;
    sm: string;
    md: string;
    lg: string;
};


interface partialHero{
    id: string;
    image: string;
    name: string;
    info: {
        intelligence: number,
        speed: number,
        power: number,
        gender: string,
        race: string
    }
}

export { hero , partialHero }